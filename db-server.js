const { db } = require('./db')
const ipc = require('node-ipc')

ipc.config.id = 'db-server'
ipc.config.retry = 1500

ipc.serveNet('udp4', function () {
  ipc.server.on('message', function (data, socket) {
    const { from, type, params } = data
    ipc.log('got a message from [', from, '] : ', data)
    if (type === 'createCollection') {
      const result = db.createCollection(params.name)
      ipc.server.emit(socket, 'message', {
        from: ipc.config.id,
        message: result.message
      })
    } else if (type === 'insert') {
      const result = db.insert(params.collection, params.value)
      ipc.server.emit(socket, 'message', {
        from: ipc.config.id,
        message: result.message
      })
    } else if (type === 'get') {
      const result = db.get(params.collection, params.key, params.value)
      ipc.server.emit(socket, 'message', {
        from: ipc.config.id,
        message: result.message
      })
    } else if (type === 'update') {
      const result = db.update(params.collection, params.id, params.value)
      ipc.server.emit(socket, 'message', {
        from: ipc.config.id,
        message: result.message
      })
    } else if (type === 'delete') {
      const result = db.delete(params.collection, params.id)
      ipc.server.emit(socket, 'message', {
        from: ipc.config.id,
        message: result.message
      })
    } else {
      ipc.server.emit(socket, 'message', {
        from: ipc.config.id,
        message: `Server is listening`
      })
    }
  })

  console.log(ipc.server)
})

ipc.server.start()
