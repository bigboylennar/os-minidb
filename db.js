class Database {
  constructor () {
    this.data = {}
  }

  createCollection (name) {
    this.data[name] = []
    return {
      message: `${name} is created`
    }
  }

  insert (collection, value) {
    if (this.data[collection]) {
      const collectionLength = this.data[collection].length
      const docId = collectionLength
        ? this.data[collection][collectionLength - 1].id + 1
        : 1
      const doc = { id: docId, ...value }
      this.data[collection].push(doc)
      return {
        doc,
        message: `${JSON.stringify(doc)} inserted to ${collection}`
      }
    }
    return {
      message: `Collection ${collection} does not exist`
    }
  }

  get (collection, key, value) {
    if (this.data[collection] || (key && value)) {
      let result = []
      if (key && value) {
        result = this.data[collection].filter(doc => doc[key] === value)
      } else {
        result = this.data[collection]
      }
      return {
        doc: result,
        message: result.length
          ? `Read successful: ${JSON.stringify(result)}`
          : 'Document does not exist'
      }
    }
    return {
      message: `Collection ${collection} does not exist`
    }
  }

  update (collection, id, value) {
    if (this.data[collection]) {
      const index = this.data[collection].findIndex(
        document => document.id == id
      )
      if (index < 0) {
        return {
          message: `Document does not exist`
        }
      }
      const doc = {
        ...this.data[collection][index],
        ...value,
        id: this.data[collection][index].id
      }
      this.data[collection].splice(index, 1, doc)
      return {
        doc,
        message: `${JSON.stringify(doc)} updated in ${collection}`
      }
    }
    return {
      message: "Collection doesn't exist"
    }
  }

  delete (collection, id) {
    if (this.data[collection]) {
      const index = this.data[collection].findIndex(
        document => document.id == id
      )
      if (index < 0) {
        return {
          message: `Document does not exist`
        }
      }
      this.data[collection].splice(index, 1)
      return {
        doc,
        message: `${JSON.stringify(doc)} deleted in ${collection}`
      }
    }
    return {
      message: "Collection doesn't exist"
    }
  }
}

module.exports = {
  db: new Database()
}
