const ipc = require('node-ipc')

ipc.config.id = 'db-client'
ipc.config.retry = 1500

ipc.serveNet(8001, 'udp4', function () {
  ipc.server.on('message', function (data) {
    ipc.log('A message from [', data.from, '] : ', data.message)
  })

  ipc.server.emit(
    // create collection
    {
      address: 'localhost',
      port: ipc.config.networkPort
    },
    'message',
    {
      from: ipc.config.id,
      type: 'createCollection',
      params: {
        name: 'Dogs'
      }
    }
  )

  ipc.server.emit(
    // insert document success
    {
      address: 'localhost',
      port: ipc.config.networkPort
    },
    'message',
    {
      from: ipc.config.id,
      type: 'insert',
      params: {
        collection: 'Dogs',
        value: {
          name: 'Spike',
          age: 3
        }
      }
    }
  )

  ipc.server.emit(
    // insert document success
    {
      address: 'localhost',
      port: ipc.config.networkPort
    },
    'message',
    {
      from: ipc.config.id,
      type: 'insert',
      params: {
        collection: 'Dogs',
        value: {
          name: 'Johnny',
          age: 5
        }
      }
    }
  )

  ipc.server.emit(
    // insert document fail
    {
      address: 'localhost',
      port: ipc.config.networkPort
    },
    'message',
    {
      from: ipc.config.id,
      type: 'insert',
      params: {
        collection: 'Cats',
        value: {
          name: 'Meyaw',
          age: 1
        }
      }
    }
  )

  ipc.server.emit(
    // get all document in a collection
    {
      address: 'localhost',
      port: ipc.config.networkPort
    },
    'message',
    {
      from: ipc.config.id,
      type: 'get',
      params: {
        collection: 'Dogs'
      }
    }
  )

  ipc.server.emit(
    // get document with id : 2
    {
      address: 'localhost',
      port: ipc.config.networkPort
    },
    'message',
    {
      from: ipc.config.id,
      type: 'get',
      params: {
        collection: 'Dogs',
        key: 'id',
        value: 2
      }
    }
  )

  ipc.server.emit(
    // get document with name : Spike
    {
      address: 'localhost',
      port: ipc.config.networkPort
    },
    'message',
    {
      from: ipc.config.id,
      type: 'get',
      params: {
        collection: 'Dogs',
        key: 'name',
        value: 'Spike'
      }
    }
  )

  ipc.server.emit(
    // get document fail
    {
      address: 'localhost',
      port: ipc.config.networkPort
    },
    'message',
    {
      from: ipc.config.id,
      type: 'get',
      params: {
        collection: 'Dogs',
        key: 'name',
        value: 'Brownies'
      }
    }
  )

  ipc.server.emit(
    // update document with id : 2
    {
      address: 'localhost',
      port: ipc.config.networkPort
    },
    'message',
    {
      from: ipc.config.id,
      type: 'update',
      params: {
        collection: 'Dogs',
        id: 2,
        value: {
          name: 'Brownies'
        }
      }
    }
  )

  ipc.server.emit(
    // update document fail
    {
      address: 'localhost',
      port: ipc.config.networkPort
    },
    'message',
    {
      from: ipc.config.id,
      type: 'update',
      params: {
        collection: 'Dogs',
        id: 5,
        value: {
          name: 'Brownies'
        }
      }
    }
  )
})

ipc.server.start()
